use std::{fs::{OpenOptions, self, File}, io::Write, path::PathBuf, collections::HashMap};

use directories::BaseDirs;

use crate::kana::{Kana, KanaStats, KanaType::{Hiragana, Katakana}};

pub fn parsing_error_text(token: &str, index: usize, file: &str) -> String {
    format!("error while parsing {token} at line {index} in {file} file")
}

pub fn save_stats_to_file(stats: &HashMap<Kana, KanaStats>) {

    let mut file = OpenOptions::new()
        .write(true)
        .open(SABI_DATA_FILE.as_path())
        .expect("error opening sabi data file");

    for (k, ks) in stats {
        if let Err(err) = file.write_all(format!("{};{};{};{}\n", k.kana, ks.total_guesses, ks.failed_attempts, ks.time).as_bytes()) {
            panic!("{}", err)
        }
    }
}

pub fn load_stats() -> HashMap<Kana, KanaStats> {
    let file_contents = fs::read_to_string(SABI_DATA_FILE.as_path())
        .expect("failed reading sabi data file");
    
    let mut ret = HashMap::new();
    KANA.iter()
        .for_each(|k| { ret.insert(k.clone(), KanaStats::new()); });

    for (index, line) in file_contents.split('\n').enumerate() {

        if line.trim().is_empty() || line.trim_start().starts_with('#') { continue; }

        let mut semicolon_split = line.split(';');

        let kana = semicolon_split.next()
            .expect(&parsing_error_text("kana", index, "stats"))
            .to_string()
            .chars()
            .next()
            .expect(&parsing_error_text("kana", index, "stats"));

        let kana = KANA.iter().find(|k| k.kana == kana)
            .expect(&parsing_error_text("kana", index, "stats"));

        let total_guesses = semicolon_split.next()
            .expect(&parsing_error_text("total_guesses", index, "stats"))
            .parse::<u128>()
            .expect(&parsing_error_text("total_guesses", index, "stats"));

        let failed_attempts = semicolon_split.next()
            .expect(&parsing_error_text("failed_attempts", index, "stats"))
            .parse::<u128>()
            .expect(&parsing_error_text("failed_attempts", index, "stats"));

        let time = semicolon_split.next()
            .expect(&parsing_error_text("time", index, "stats"))
            .parse::<f64>()
            .expect(&parsing_error_text("time", index, "stats"));

        ret.insert(kana.clone(), KanaStats { total_guesses, failed_attempts, time});
    }

    ret
}

lazy_static! {
    pub static ref SABI_DATA_FILE: PathBuf = {
        let data_dir = BaseDirs::new().expect("error getting base dirs").data_dir().join("sabi/");
        let _ = fs::create_dir(&data_dir);
        let data_file = data_dir.join("stats");
        let _ = File::create_new(&data_file);
        data_file
    };
    // file format:
    // h;k;ro,ma,ji
    // h is the hiragana unicode character
    // k is the katakana unicode character
    // ro,ma,ji are all romaji representations of the kana
    // split by a comma
    // lines that start with # are ignored
    pub static ref KANA: Vec<Kana> = {
        let kana_file = include_str!("../kana");

        let mut ret = vec![];

        for (index, line) in kana_file.split('\n').enumerate() {

            if line.trim().is_empty() || line.trim_start().starts_with('#') { continue; }

            let mut semicolon_split = line.split(';');

            let hiragana = semicolon_split.next()
                .expect(&parsing_error_text("hiragana", index, "kana"))
                .chars()
                .next()
                .expect(&parsing_error_text("hiragana", index, "kana"));

            let katakana = semicolon_split.next()
                .expect(&parsing_error_text("katakana", index, "kana"))
                .chars()
                .next()
                .expect(&parsing_error_text("katakana", index, "kana"));

            let romaji: Vec<String> = semicolon_split.next()
                .expect(&parsing_error_text("romaji", index, "kana"))
                .split(',')
                .map(|s| s.to_string())
                .collect();

            ret.push(Kana { kana_type: Hiragana, kana: hiragana, romaji: romaji.clone() });
            ret.push(Kana { kana_type: Katakana, kana: katakana, romaji });
        }

        ret
    };
}
