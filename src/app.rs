use crate::{kana::{Kana, KanaStats, KanaType}, util::{KANA, save_stats_to_file, load_stats}};

use std::{time::Instant, collections::HashMap, fs, path::PathBuf};

use eframe::egui::{self, epaint::Rgba, RichText};

fn setup_custom_fonts(ctx: &egui::Context, font_path: PathBuf) {

    let font_file_contents = fs::read(font_path).expect("error loading font file");

    let mut fonts = egui::FontDefinitions::default();

    fonts.font_data.insert(
        "jp".into(),
        egui::FontData::from_owned(font_file_contents),
    );

    fonts.families
        .entry(egui::FontFamily::Proportional)
        .or_default()
        .insert(0, "jp".into());

    ctx.set_fonts(fonts);
}

pub struct Sabi {
    stats: HashMap<Kana, KanaStats>,
    current_stats: KanaStats,
    timer: Instant,
    answer: String,
    last_answer: String,
    answer_status: Option<bool>,
    hiragana: bool,
    katakana: bool,
    current_kana: Kana,
    display_char: char,
}

impl Sabi {
    pub fn new(cc: &eframe::CreationContext<'_>, font_path: Option<PathBuf>) -> Self {

        if font_path.is_some() && font_path.clone().unwrap().exists() {
            setup_custom_fonts(&cc.egui_ctx, font_path.unwrap());
        }

        let current_kana = Kana::random_kana(Some(KanaType::Hiragana));
        let display_char = current_kana.kana;

        Self {
            stats: load_stats(),
            current_stats: KanaStats::new(),
            timer: Instant::now(),
            answer: Default::default(),
            last_answer: Default::default(),
            answer_status: None,
            hiragana: true,
            katakana: false,
            current_kana,
            display_char,
        }
    }

    fn pick_new_kana(&mut self) {
        let kana_type = if self.hiragana && self.katakana { None }
            else if self.hiragana { Some(KanaType::Hiragana) }
            else { Some(KanaType::Katakana) };

        let mut new_kana = Kana::random_kana(kana_type.clone());
        while new_kana == self.current_kana {
            new_kana = Kana::random_kana(kana_type.clone());
        }
        self.current_kana = new_kana;
    }

    fn complete_current_guess(&mut self) {
        self.current_stats.time += self.timer.elapsed().as_secs_f64();
        self.current_stats.total_guesses += 1;

        self.stats.insert(self.current_kana.clone(), self.current_stats.clone());
    }

    fn init_new_guess(&mut self) {
        self.timer = Instant::now();

        self.answer = String::new();
        self.last_answer = Default::default();

        self.pick_new_kana();
        self.current_stats = self.stats[&self.current_kana].clone();

        self.display_char = self.current_kana.kana;
    }

    fn submit_guess(&mut self) {
        if !self.answer.is_empty() && self.answer != self.last_answer {

            let is_answer_correct = self.current_kana.matches_romaji(&self.answer);
            self.answer_status = Some(is_answer_correct);

            if is_answer_correct {
                self.complete_current_guess();
                self.init_new_guess();
            } else {
                self.current_stats.failed_attempts += 1;
                self.last_answer = self.answer.clone();
            }
        }
    }

}

// stats[kana] here is ok because we only get the stats in load_stats(), 
// which initializes all values to KanaStats::new()
fn stats(stats: &HashMap<Kana, KanaStats>, kana: Option<&Kana>) -> (u128, u128, f64, f64) {

    let correct_guesses = kana.map_or_else(
        || stats.iter()
            .map(|(_, ks)| ks.total_guesses)
            .sum(),
        |kana| stats[kana].total_guesses
    );

    let incorrect_guesses = kana.map_or_else(
        || stats.iter()
            .map(|(_, ks)| ks.failed_attempts)
            .sum(),
        |kana| stats[kana].failed_attempts
    );

    let total_time = kana.map_or_else(
        || stats.iter()
            .map(|(_, ks)| ks.time)
            .sum(),
        |kana| stats[kana].time
    );

    let average_time = total_time / correct_guesses as f64;

    (correct_guesses, incorrect_guesses, average_time, total_time)
}

impl eframe::App for Sabi {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                let hiragana_checkbox = ui.checkbox(&mut self.hiragana, "Hiragana");
                let katakana_checkbox = ui.checkbox(&mut self.katakana, "Katakana");

                if hiragana_checkbox.clicked() && !self.hiragana && !self.katakana {
                    self.hiragana = true;
                }
                if katakana_checkbox.clicked() && !self.katakana && !self.hiragana {
                    self.katakana = true;
                }

                ui.separator();

                ui.label(RichText::new(self.display_char).size(128.));

                let text_edit = ui.text_edit_singleline(&mut self.answer);

                let enter_pressed = ui.input(|i| i.key_pressed(egui::Key::Enter));

                if text_edit.lost_focus() && enter_pressed {
                    text_edit.request_focus();
                    self.submit_guess();
                }

                ui.add_space(10.);
                ui.separator();

                if let Some(status) = self.answer_status {
                    if status {
                        ui.label(RichText::new("correct! :-)").size(36.).color(Rgba::GREEN));
                    } else {
                        ui.label(RichText::new("wrong! :-(").size(36.).color(Rgba::RED));
                    }
                }

                let hint = ui.button("Hint");
                // unwrap() is ok as long as the kana file is correct
                let current_romaji = self.current_kana.default_romaji();

                hint.clone().on_hover_text_at_pointer(&current_romaji);

                if hint.clicked() {
                    self.answer = current_romaji.to_string();
                    text_edit.request_focus();
                }

                ui.collapsing("Sabi Stats", |ui| {
                    egui::ScrollArea::vertical().show(ui, |ui| {

                        let (correct_guesses, incorrect_guesses, average_time, total_time) = stats(&self.stats, None);

                        ui.horizontal(|ui| {
                            ui.label(RichText::new("all:").heading());
                            ui.label(format!("correct/all guesses: {}/{};", correct_guesses, correct_guesses + incorrect_guesses));
                            ui.label(format!("average/total time: {}s/{}s;", average_time as u64, total_time as u64));

                            ui.separator();

                            if ui.button("reset stats").clicked() {
                                KANA.iter()
                                    .for_each(|k| { self.stats.insert(k.clone(), KanaStats::new()); });
                            }
                        });

                        ui.separator();

                        ui.horizontal(|ui| {
                            ui.vertical(|ui| {
                                KANA.iter()
                                    .filter(|k| k.kana_type == KanaType::Hiragana)
                                    .for_each(|hiragana| {
                                        let (h_correct_guesses, h_incorrect_guesses, h_average_time, h_total_time) = stats(&self.stats, Some(hiragana));
                                        ui.horizontal(|ui| {
                                            ui.label(RichText::new(format!("{}:", hiragana.kana)).heading());
                                            ui.label(format!("correct/all guesses: {}/{};", h_correct_guesses, h_correct_guesses + h_incorrect_guesses));
                                            ui.label(format!("average/total time: {}s/{}s;", h_average_time as u64, h_total_time as u64));
                                        });
                                    });
                            });
                            ui.separator();
                            ui.vertical(|ui| {
                                KANA.iter()
                                    .filter(|k| k.kana_type == KanaType::Katakana)
                                    .for_each(|katakana| {
                                        let (k_correct_guesses, k_incorrect_guesses, k_average_time, k_total_time) = stats(&self.stats, Some(katakana));
                                        ui.horizontal(|ui| {
                                            ui.label(RichText::new(format!("{}:", katakana.kana)).heading());
                                            ui.label(format!("correct/all guesses: {}/{};", k_correct_guesses, k_correct_guesses + k_incorrect_guesses));
                                            ui.label(format!("average/total time: {}s/{}s;", k_average_time as u64, k_total_time as u64));
                                        });
                                    });
                            });
                        });
                    });
                });
            });
        });
    }
    fn on_close_event(&mut self) -> bool {
        save_stats_to_file(&self.stats);

        true
    }
}
