use rand::seq::SliceRandom;

use crate::util::KANA;

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum KanaType {
    Hiragana,
    Katakana,
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Kana {
    pub kana: char,
    pub kana_type: KanaType,
    pub romaji: Vec<String>,
}

impl Kana {
    // None - both
    // Some(t) - t
    pub fn random_kana(kana_type: Option<KanaType>) -> Self {
        // unwrap() is ok as long as the kana file is correct
        KANA.iter()
            .filter(|k| if kana_type.is_some() { k.kana_type == kana_type.clone().unwrap() } else { true })
            .collect::<Vec<_>>()
            .choose(&mut rand::thread_rng()).unwrap().clone().clone()
    }

    pub fn matches_romaji(&self, romaji: &String) -> bool {
        self.romaji.contains(romaji)
    }

    pub fn default_romaji(&self) -> String {
        // romaji[0] is ok as long as the kana file is correct
        self.romaji[0].clone()
    }
}

#[derive(Clone)]
pub struct KanaStats {
    pub total_guesses: u128,
    pub failed_attempts: u128,
    pub time: f64,
}

impl KanaStats {
    pub fn new() -> Self {
        KanaStats {
            total_guesses: 0,
            failed_attempts: 0,
            time: 0.,
        }
    }
}
