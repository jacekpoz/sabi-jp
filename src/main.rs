#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
#![feature(iter_collect_into)]
#![feature(file_create_new)]

use std::{path::PathBuf, fs};

use app::Sabi;
use clap::{arg, command, Parser};
use directories::BaseDirs;
use eframe::egui;

mod app;
mod kana;
mod util;

#[macro_use]
extern crate lazy_static;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Opts {
    #[arg(long, short = 'f')]
    font_path: Option<String>,
}

fn main() -> Result<(), eframe::Error> {
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(800., 600.)),
        app_id: Some("sabi".into()),
        ..Default::default()
    };

    let opts = Opts::parse();

    let font_path = opts.font_path;

    let mut path = String::new();

    if font_path.is_none() {
        let config_file = BaseDirs::new()
            .expect("error getting base dirs")
            .config_dir()
            .join("sabi.conf");

        if config_file.exists() {
            path = match fs::read_to_string(config_file) {
                Ok(contents) => { contents.trim().to_string() },
                Err(_) => { String::new() },
            };
        }
    } else {
        path = font_path.unwrap().trim().to_string();
    }

    eframe::run_native(
        "Sabi",
        options,
        Box::new(|cc| Box::new(Sabi::new(cc, Some(PathBuf::from(path))))),
    )
}

