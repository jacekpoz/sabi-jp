# sabi-jp
small desktop program for learning hiragana and katakana, heavily inspired by [Nihonoari](https://github.com/aeri/Nihonoari-App)

# install
get the binary in the [releases](https://codeberg.org/jacekpoz/sabi-jp/releases) tab

only available on `x86_64-unknown-linux-gnu` and `x86_64-pc-windows-gnu` because windows msvc and mac intel didn't work
get in touch if you want me to add some platform and package it in releases

or

```
cargo install sabi-jp
```

should work on all platforms but not tested

# build
install [noto-cjk](https://github.com/notofonts/noto-cjk)
```
cp /path/to/font.ttc ./NotoSansCJK.ttc
cargo build --release
target/release/sabi
```
